// Point - helper class for pair (x, y)
//

var utils = {
    random: function(from, to) {
        return Math.floor(Math.random() * to) + from;
    }
};

function Point(x, y, c) {
    this.x = x || 0;
    this.y = y || 0;
    this.color = c || 'black';
}

function get_random_points(count, min, max) {
    var points = [], i;
    var colors = ['red', 'green', 'blue', 'yellow', 'black', 'gray', 'magenta', 'brown'];

    for (i = 0; i < count; i++) {
        points.push(
            new Point(
                utils.random(min, max),
                utils.random(min, max),
                colors[utils.random(0, colors.length)]
            )
        );
    }
    return points;
}

// Fuzzy c-Means algorithm
//

function FCM(items, options) {
    'use strict';

    // default algorithm parameters
    this.options = {
        // algorithm accuracy (epsilon)
        eps: 0.1,

        // max iteration count
        iterations: 150,

        // number of objects for clustering
        object_count: items.length,

        // the number of clusters to split
        clusters_count: 4,

        // fuzziness factor
        fuzziness: 1.5
    };

    this.objects = items;

    // extend default options by users parameters
    $.extend(this.options, options);
}

FCM.prototype = {
    run: function () {
        console.time('FCM routine');

        var centers = get_random_points(this.options.clusters_count);
        var matrix = this._fill_belonging_matrix();

        var previous_decision_val = 0,
            current_decision_val = 1,
            index, distance,
            self = this;

        for (index = 0;
             index < this.options.iterations && (Math.abs(previous_decision_val - current_decision_val) > this.options.eps);
             index++) {
            previous_decision_val = current_decision_val;
            centers = this._calculate_centers(matrix);
            $.each(matrix, function (key, row) {
                $.each(row, function (cluster_index) {
                    distance = self._euclid_distance(self.objects[key], centers[cluster_index]);
                    row[cluster_index] = self._prepare_u(distance);
                });

                matrix[key] = self._normalize_belonging_matrix_row(row);
            });

            current_decision_val = this._calculate_decision_function(this.objects, centers, matrix);
        }

        console.timeEnd('FCM routine');
        return matrix;
    },

    _fill_belonging_matrix: function () {
        var matrix = [], i, j;

        for (i = 0; i < this.options.object_count; i++) {
            matrix[i] = [];
            for (j = 0; j < this.options.clusters_count; j++) {
                matrix[i][j] = Math.random();
            }
            matrix[i] = this._normalize_belonging_matrix_row(matrix[i]);
        }

        return matrix;
    },

    _normalize_belonging_matrix_row: function (row) {
        var sum = 0;

        $.each(row, function (i, v) {
            sum += v;
        });
        return $.map(row, function (v) {
            return v / sum;
        });
    },

    _calculate_centers: function (matrix) {
        var m = this.options.fuzziness,
            centroids = [], index,
            temp_ax, temp_ay,
            temp_bx, temp_by,
            self = this;

        for (index = 0; index < this.options.clusters_count; index++) {
            temp_ax = 0; temp_ay = 0;
            temp_bx = 0; temp_by = 0;

            $.each(matrix, function (i, row) {
                temp_ax += Math.pow(row[index], m);
                temp_bx += Math.pow(row[index], m) * self.objects[i].x;

                temp_ay += Math.pow(row[index], m);
                temp_by += Math.pow(row[index], m) * self.objects[i].y;
            });

            centroids[index] = new Point();
            centroids[index].x = temp_bx / temp_ax;
            centroids[index].y = temp_by / temp_ay;
        }

        return centroids;
    },

    _calculate_decision_function: function (matrix_point_x, centroids, belonging_matrix) {
        var sum = 0, self = this;
        $.each(belonging_matrix, function (index, row) {
            $.each(row, function (cluster_index, u) {
                sum += u * self._euclid_distance(centroids[cluster_index], matrix_point_x[index]);
            });
        });

        return sum;
    },

    _prepare_u: function (distance) {
        return Math.pow(1 / distance, 2 / (this.options.fuzziness - 1));
    },

    _euclid_distance: function (a, b) {
        return Math.sqrt(
            Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)
        );
    }
};

