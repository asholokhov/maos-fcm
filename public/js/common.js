(function(){
    'use strict';

    var canvas = window.__canvas =  new fabric.StaticCanvas('cluster-map', { width: 430, height: 400 });

    var chart = {
        draw: function(points){
            console.time('Draw routine');
            canvas.clear();

            $.each(points, function(i, v){
                canvas.add(
                    new fabric.Circle({ top: v.y, left: v.x, radius: 2, fill: v.color })
                );
            });

            canvas.renderAll();
            console.timeEnd('Draw routine');
        },

        drawClusters: function(matrix, points){
            __canvas.clear();

            var x_factor = __canvas.width,
                y_factor = __canvas.height,
                cluster  = 0,
                max = 0,
                colors = ['red', 'green', 'blue', 'gold', 'black', 'gray', 'magenta', 'brown'];

            $.each(matrix, function (i, row) {
                cluster = 0;
                max = 0;

                $.each(row, function(i, v){
                    if (v > max) {
                        max = v;
                        cluster = i;
                    }
                });

                canvas.add(
                    new fabric.Circle({top: points[i].y, left: points[i].x, radius: 2, fill: colors[cluster]})
                );
            });

            __canvas.renderAll();
        }
    };

    var startup = function(){
        var points = [];

        $('button[role="generate"]').click(function(e){
            e.preventDefault();

            var count = +$('#points_count').val();
            points = get_random_points(count, 0, __canvas.width);
            chart.draw(points);

            console.log('generated and drawed points', points);
        });

        $('button[role="cluster"]').click(function(e){
            e.preventDefault();

            var fcm = new FCM(points, {
                clusters_count: +$('#clusters_count').val(),
                eps: +$('#eps').val(),
                iterations: +$('#iter_count').val(),
                fuzziness: +$('#fuzzy_fact').val()
            });

            var matrix = fcm.run();
            chart.drawClusters(matrix, points);

            console.log('result matrix', matrix);
        });
    };

    startup();
})();